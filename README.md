# Project name
The Goose Game Kata

## Compile game

JDK version 1.8 is required to build the application.
To compile and build the project follow this steps:

1. `git clone` the project in a directory.
2. `./gradlew clean build` on the project directory.

## Play game

To start game go to **build/libs** directory under project directory and execute `java -jar goose-game-<GRADLE_VERSION>.jar`

### Game commands

The commands are:

- `add player <PLAYER_NAME>` : add player to game.
- `move <PLAYER_NAME>` : rolls two dice and move player.
- `quit` : quit the game.

### Dev Environment

The entire development was made with the following tools version:

Java version "1.8.0_201"

Gradle 4.10.3