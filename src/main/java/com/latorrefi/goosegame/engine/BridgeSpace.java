package com.latorrefi.goosegame.engine;

import java.util.function.Function;

public class BridgeSpace extends BasicSpace {


    public BridgeSpace(String name, Integer index) {
        super(name, index);
    }

    @Override
    public Function<Integer, Integer> getRule() {
        return (roll) -> 12;
    }
}
