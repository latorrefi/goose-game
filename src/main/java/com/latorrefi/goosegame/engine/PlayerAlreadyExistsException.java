package com.latorrefi.goosegame.engine;

public class PlayerAlreadyExistsException extends Exception{

    public PlayerAlreadyExistsException(String name) {
        super("Player " + name + " already exist");
    }
}
