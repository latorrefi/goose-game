package com.latorrefi.goosegame.engine;

import java.util.List;
import java.util.Set;

public interface GameStatusListener {

    void onPlayerAdd(Set<String> playersName);

    void onPlayerRolls(String playerName, List<Integer> rolls);

    void onPlayerMoved(String playerName, Space from, Space to);

    void onPlayerBounced(String playerName, Space to);

    void onPlayerWin(String playerName);

    void onPlayerJump(String playerName, Space to);

}
