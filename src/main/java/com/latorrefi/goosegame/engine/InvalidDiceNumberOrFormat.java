package com.latorrefi.goosegame.engine;

import java.util.List;

public class InvalidDiceNumberOrFormat extends Exception {

    public InvalidDiceNumberOrFormat(List<Integer> dice) {
        super("Invalid dice number or format " + dice);
    }
}
