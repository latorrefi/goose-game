package com.latorrefi.goosegame.engine;

import java.util.function.Function;

public class BasicSpace implements Space {

    private final String name;
    private final Integer index;

    public BasicSpace(String name, Integer index) {
        this.name = name;
        this.index = index;
    }

    @Override
    public String getName() {
        return name;
    }

    public Integer getIndex() {
        return index;
    }

    @Override
    public Function<Integer, Integer> getRule() {
        return (dice) -> index;
    }
}
