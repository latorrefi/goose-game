package com.latorrefi.goosegame.engine;

import java.util.*;

import static com.latorrefi.goosegame.engine.Dice.MAX_DICE_NUMBER;
import static com.latorrefi.goosegame.engine.Dice.MAX_DICE_VALUE;

public class GameContext {

    private Map<String, Integer> players = new HashMap<>();
    private Dice diceGenerator;
    private GameStatusListener gameStatusListener;

    public GameContext(GameStatusListener gameStatusListener, Dice dice) {
        this.gameStatusListener = gameStatusListener;
        this.diceGenerator = dice;
    }

    public GameContext(GameStatusListener gameStatusListener) {
        this.gameStatusListener = gameStatusListener;
        this.diceGenerator = new Dice();
    }

    public void addPlayer(String playerName) throws PlayerAlreadyExistsException,
            InvalidPlayerNameException {
        //Start validation
        if (playerName == null) {
            throw new InvalidPlayerNameException();
        }
        if (players.containsKey(playerName)) {
            throw new PlayerAlreadyExistsException(playerName);
        }
        //
        players.put(playerName, 0);
        gameStatusListener.onPlayerAdd(getPlayersName());
    }

    protected Set<String> getPlayersName() {
        return Collections.unmodifiableSet(players.keySet());
    }

    public Integer getPlayerSpaceIndex(String playerName) throws PlayerNotFoundException {
        Integer spaceIndex = players.get(playerName);
        if (spaceIndex == null) {
            throw new PlayerNotFoundException(playerName);
        }
        return spaceIndex;
    }

    public void movePlayer(String playerName, List<Integer> dice) throws PlayerNotFoundException,
            InvalidDiceNumberOrFormat {
        Integer currentSpaceIndex = this.getPlayerSpaceIndex(playerName);

        validateDiceInput(dice);


        //Generate random rolls
        if (dice == null) {
            dice = diceGenerator.random();
        }

        gameStatusListener.onPlayerRolls(playerName, dice);
        //Sum dice values
        Integer rollsSum = dice.stream()
                .mapToInt(Integer::intValue)
                .sum();


        //Sum rolls value to currentSpace
        Integer newSpaceIndex = currentSpaceIndex + rollsSum;

        //Retrieve information about current space
        Space currentSpace = SpaceFactory.getSpace(currentSpaceIndex);
        gameStatusListener.onPlayerMoved(playerName, currentSpace,
                SpaceFactory.getSpace(Math.min(newSpaceIndex, SpaceFactory.VICTORY_SPACE)));

        applyRule(playerName, rollsSum, newSpaceIndex);
    }

    private void applyRule(String playerName, Integer rollsSum, Integer newSpaceIndex) {
        if (newSpaceIndex > SpaceFactory.VICTORY_SPACE) {
            newSpaceIndex = 2 * SpaceFactory.VICTORY_SPACE - newSpaceIndex;
            gameStatusListener.onPlayerBounced(playerName, SpaceFactory.getSpace(newSpaceIndex));
        } else if (newSpaceIndex.intValue() == SpaceFactory.VICTORY_SPACE.intValue()) {
            gameStatusListener.onPlayerWin(playerName);

        }
        //Retrieve information about new space
        Space newSpace = SpaceFactory.getSpace(newSpaceIndex);
        //update player position on board
        Integer spaceIndexAfterRuleApply = newSpace.getRule().apply(rollsSum);
        if (!spaceIndexAfterRuleApply.equals(newSpaceIndex)) {
            gameStatusListener.onPlayerJump(playerName, SpaceFactory.getSpace(spaceIndexAfterRuleApply));
            applyRule(playerName, rollsSum, spaceIndexAfterRuleApply);
        }

        players.put(playerName, spaceIndexAfterRuleApply);
    }

    private void validateDiceInput(List<Integer> dice) throws InvalidDiceNumberOrFormat {
        //We are playing with 2 dice with number from 1 to 6
        if ((dice != null && dice.size() != MAX_DICE_NUMBER) ||
                (dice != null && dice.get(0) == null) ||
                (dice != null && dice.get(1) == null) ||
                dice != null && (dice.get(0) < 1 || dice.get(0) > MAX_DICE_VALUE) ||
                dice != null && (dice.get(1) < 1 || dice.get(1) > MAX_DICE_VALUE)
        ) {
            throw new InvalidDiceNumberOrFormat(dice);
        }
    }

}
