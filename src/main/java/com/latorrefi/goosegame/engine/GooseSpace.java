package com.latorrefi.goosegame.engine;

import java.util.function.Function;

public class GooseSpace extends BasicSpace {

    public GooseSpace(String name, Integer index) {
        super(name, index);
    }

    @Override
    public Function<Integer, Integer> getRule() {
        return (Integer roll) -> getIndex() + roll;
    }

}
