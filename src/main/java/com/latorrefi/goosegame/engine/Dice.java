package com.latorrefi.goosegame.engine;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Dice {

    public static final Integer MAX_DICE_VALUE = 6;
    public static final Integer MAX_DICE_NUMBER = 2;

    public List<Integer> random() {
        Random random = new Random();
        return random.ints(MAX_DICE_NUMBER, 1, MAX_DICE_VALUE + 1)
                .boxed()
                .collect(Collectors.toList());
    }
}
