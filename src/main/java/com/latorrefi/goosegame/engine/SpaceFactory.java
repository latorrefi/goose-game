package com.latorrefi.goosegame.engine;

public class SpaceFactory {

    public static final Integer VICTORY_SPACE = 63;

    public static Space getSpace(Integer index) {
        switch (index) {
            case 0:
                return new BasicSpace("Start", index);
            case 6:
                return new BridgeSpace("The Bridge", index);
            case 5:
            case 9:
            case 14:
            case 18:
            case 23:
            case 27:
                return new GooseSpace(index + ", The Goose", index);
            default:
                return new BasicSpace(Integer.toString(index), index);
        }
    }
}
