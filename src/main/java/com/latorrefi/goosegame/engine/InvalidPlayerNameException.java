package com.latorrefi.goosegame.engine;

public class InvalidPlayerNameException extends Exception {

    public InvalidPlayerNameException() {
        super("Player name not valid");
    }
}
