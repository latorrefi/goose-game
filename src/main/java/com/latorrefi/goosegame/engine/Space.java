package com.latorrefi.goosegame.engine;

import java.util.function.Function;

public interface Space {

    String getName();

    Function<Integer, Integer> getRule();
}
