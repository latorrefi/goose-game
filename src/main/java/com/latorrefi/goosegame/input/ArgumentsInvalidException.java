package com.latorrefi.goosegame.input;

import java.util.List;

public class ArgumentsInvalidException extends Exception {

    public ArgumentsInvalidException(String command, List<String> arguments) {
        super("Command " + command + " have invalid arguments: " + arguments);
    }

    public ArgumentsInvalidException(String msg) {
        super(msg);
    }
}
