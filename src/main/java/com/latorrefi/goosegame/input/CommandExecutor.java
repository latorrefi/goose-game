package com.latorrefi.goosegame.input;

import com.latorrefi.goosegame.engine.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CommandExecutor {

    private static final String EMPTY_STRING = "";
    private static final String SPACE_STRING = " ";

    //Valid command
    private static final String ADD_PLAYER = "add player";
    private static final String QUIT = "quit";
    private static final String MOVE = "move";


    private List<String> commands = Arrays.asList(ADD_PLAYER, MOVE, QUIT);

    private final GameContext gameContext;

    public CommandExecutor(GameContext gameContext) {
        this.gameContext = gameContext;
    }

    public CommandExecutor(GameStatusListener gameStatusListener) {
        this.gameContext = new GameContext(gameStatusListener);
    }

    public void execute(String input) throws CommandNotFoundException,
            ArgumentsInvalidException,
            PlayerAlreadyExistsException,
            QuitGameException,
            InvalidPlayerNameException,
            PlayerNotFoundException {
        if (input == null) {
            throw new QuitGameException();
        }
        //Discover command in input
        String command = commands.stream().filter(input::startsWith)
                .findFirst()
                .orElseThrow(() -> new CommandNotFoundException(input));
        //Here we are sure command exists and we have to found arguments
        String argumentsRaw = input.replace(command, EMPTY_STRING).trim();
        List<String> arguments = Arrays.asList(argumentsRaw.split(SPACE_STRING));
        //Update game context
        switch (command) {
            case ADD_PLAYER:
                handleAddPlayer(arguments);
                break;
            case MOVE:
                handleMovePlayer(arguments);
                break;
            case QUIT:
                throw new QuitGameException();
            default:
                //We should never arrive here
                throw new CommandNotFoundException(input);
        }
    }

    private void handleMovePlayer(List<String> arguments) throws ArgumentsInvalidException,
            PlayerNotFoundException {
        if (arguments == null ||
                arguments.size() == 0 ||
                arguments.size() > 2 ||
                EMPTY_STRING.equals(arguments.get(0))
        ) {
            throw new ArgumentsInvalidException(MOVE, arguments);
        }
        List<Integer> dice = null;
        if (arguments.size() == 2) {
            try {
                dice = Arrays.stream(arguments.get(1).split(","))
                        .map(Integer::parseInt).collect(Collectors.toList());
            } catch (Exception ex) {
                throw new ArgumentsInvalidException(ex.getMessage());
            }
        }
        try {
            gameContext.movePlayer(arguments.get(0), dice);
        } catch (InvalidDiceNumberOrFormat invalidDiceNumberOrFormat) {
            throw new ArgumentsInvalidException(invalidDiceNumberOrFormat.getMessage());
        }
    }

    private void handleAddPlayer(List<String> arguments) throws ArgumentsInvalidException,
            PlayerAlreadyExistsException,
            InvalidPlayerNameException {
        if (arguments.size() != 1) {
            throw new ArgumentsInvalidException(ADD_PLAYER, arguments);
        }
        gameContext.addPlayer(arguments.get(0));
    }

}
