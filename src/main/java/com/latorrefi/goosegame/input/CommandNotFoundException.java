package com.latorrefi.goosegame.input;

public class CommandNotFoundException extends Exception{

    public CommandNotFoundException(String userString) {
        super("Command " + userString + " not found");
    }
}
