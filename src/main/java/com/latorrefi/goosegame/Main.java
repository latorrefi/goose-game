package com.latorrefi.goosegame;

import com.latorrefi.goosegame.engine.InvalidPlayerNameException;
import com.latorrefi.goosegame.engine.PlayerAlreadyExistsException;
import com.latorrefi.goosegame.engine.PlayerNotFoundException;
import com.latorrefi.goosegame.input.ArgumentsInvalidException;
import com.latorrefi.goosegame.input.CommandExecutor;
import com.latorrefi.goosegame.input.CommandNotFoundException;
import com.latorrefi.goosegame.input.QuitGameException;
import com.latorrefi.goosegame.ouput.PrinterGameStatusListener;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Play The Goose Game!");
        System.out.println("Type a command: ");
        CommandExecutor commandExecutor = new CommandExecutor(new PrinterGameStatusListener(System.out));
        try (Scanner scanner = new Scanner(System.in)) {
            boolean activeGame = true;
            do {

                String input = scanner.nextLine();
                try {
                    commandExecutor.execute(input);
                } catch (CommandNotFoundException |
                        ArgumentsInvalidException |
                        PlayerAlreadyExistsException |
                        InvalidPlayerNameException |
                        PlayerNotFoundException e) {
                    System.out.println(e.getMessage());
                } catch (QuitGameException e) {
                    activeGame = false;
                } catch (Exception e) {
                    System.out.println("Error during the game");
                    activeGame = false;
                }
                System.out.println();

            } while (activeGame);
        }

        System.out.println("Exit The Goose Game!");
    }
}
