package com.latorrefi.goosegame.ouput;

import com.latorrefi.goosegame.engine.GameStatusListener;
import com.latorrefi.goosegame.engine.Space;

import java.io.PrintStream;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class PrinterGameStatusListener implements GameStatusListener {

    private final PrintStream printStream;

    public PrinterGameStatusListener(PrintStream printStream) {
        this.printStream = printStream;
    }

    @Override
    public void onPlayerAdd(Set<String> playersName) {
        printStream.print("players: " + playersName);
    }

    @Override
    public void onPlayerRolls(String playerName, List<Integer> rolls) {
        String rollsString = rolls.stream()
                .map(String::valueOf)
                .collect(Collectors.joining(", "));
        printStream.print(playerName + " rolls " + rollsString + ". ");
    }

    @Override
    public void onPlayerMoved(String playerName, Space from, Space to) {
        printStream.print(playerName
                + " moves from "
                + from.getName()
                + " to " + to.getName() + ". ");
    }

    @Override
    public void onPlayerBounced(String playerName, Space to) {
        printStream.print(playerName + " bounces! " + playerName + " returns to " + to.getName() + ". ");
    }

    @Override
    public void onPlayerWin(String playerName) {
        printStream.print(playerName + " Wins!!");
    }

    @Override
    public void onPlayerJump(String playerName, Space to) {
        printStream.print(playerName + " jumps to " + to.getName() + ". ");
    }
}
