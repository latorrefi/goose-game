package com.latorrefi.goosegame.input;

import com.latorrefi.goosegame.engine.*;
import com.latorrefi.goosegame.ouput.PrinterGameStatusListener;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class CommandExecutorTest {


    @Mock
    private GameContext gameContext;

    @Mock
    private PrinterGameStatusListener printerGameStatusListener;

    private CommandExecutor commandExecutor;

    @Before
    public void setUp() {
        commandExecutor = new CommandExecutor(gameContext);
    }

    @Test(expected = QuitGameException.class)
    public void testExecuteWithInputNull() throws ArgumentsInvalidException,
            PlayerAlreadyExistsException,
            InvalidPlayerNameException,
            QuitGameException,
            CommandNotFoundException,
            PlayerNotFoundException {
        //Given
        //null

        //When
        commandExecutor.execute(null);

        //Then
        //expected QuitGameException
    }

    @Test(expected = CommandNotFoundException.class)
    public void testExecuteWithWrongCommand() throws ArgumentsInvalidException,
            PlayerAlreadyExistsException,
            InvalidPlayerNameException,
            QuitGameException,
            CommandNotFoundException,
            PlayerNotFoundException {
        //Given
        //command = "command"

        //When
        commandExecutor.execute("command");

        //Then
        //expected CommandNotFoundException
    }

    @Test(expected = QuitGameException.class)
    public void testExecuteWithInputQuit() throws ArgumentsInvalidException,
            PlayerAlreadyExistsException,
            InvalidPlayerNameException,
            QuitGameException,
            CommandNotFoundException,
            PlayerNotFoundException {
        //Given
        //command = "quit"

        //When
        commandExecutor.execute("quit");

        //Then
        //expected QuitGameException
    }

    @Test
    public void testExecuteWithAddPlayer() throws ArgumentsInvalidException,
            PlayerAlreadyExistsException,
            InvalidPlayerNameException,
            QuitGameException,
            CommandNotFoundException,
            PlayerNotFoundException {
        //Given
        String playerName = "Pippo";
        String input = "add player " + playerName;
        doNothing().when(gameContext).addPlayer(playerName);

        //When
        commandExecutor.execute(input);

        //Then
        verify(gameContext).addPlayer(playerName);

    }

    @Test(expected = ArgumentsInvalidException.class)
    public void testExecuteWithAddPlayerWrong() throws ArgumentsInvalidException,
            PlayerAlreadyExistsException,
            InvalidPlayerNameException,
            QuitGameException,
            CommandNotFoundException,
            PlayerNotFoundException {
        //Given
        String input = "add player Pippo Pluto";

        //When
        commandExecutor.execute(input);

        //Then
        //expected ArgumentsInvalidException
    }

    @Test(expected = ArgumentsInvalidException.class)
    public void testExecuteWithMoveWrong() throws ArgumentsInvalidException,
            PlayerAlreadyExistsException,
            InvalidPlayerNameException,
            QuitGameException,
            CommandNotFoundException,
            PlayerNotFoundException {
        //Given
        String input = "move";

        //When
        commandExecutor.execute(input);

        //Then
        //expected ArgumentsInvalidException
    }

    @Test(expected = ArgumentsInvalidException.class)
    public void testExecuteWithMoveWrong2() throws ArgumentsInvalidException,
            PlayerAlreadyExistsException,
            InvalidPlayerNameException,
            QuitGameException,
            CommandNotFoundException,
            PlayerNotFoundException {
        //Given
        String input = "move Pippo 1,2 wrong";

        //When
        commandExecutor.execute(input);

        //Then
        //expected ArgumentsInvalidException
    }

    @Test(expected = ArgumentsInvalidException.class)
    public void testExecuteWithMoveWrong3() throws ArgumentsInvalidException,
            PlayerAlreadyExistsException,
            InvalidPlayerNameException,
            QuitGameException,
            CommandNotFoundException,
            PlayerNotFoundException {
        //Given
        String input = "move Pippo a,2";

        //When
        commandExecutor.execute(input);

        //Then
        //expected ArgumentsInvalidException
    }

    @Test(expected = ArgumentsInvalidException.class)
    public void testExecuteWithMoveWrong4() throws ArgumentsInvalidException,
            PlayerAlreadyExistsException,
            InvalidPlayerNameException,
            QuitGameException,
            CommandNotFoundException,
            PlayerNotFoundException {
        //Given
        String input = "move Pippo a";

        //When
        commandExecutor.execute(input);

        //Then
        //expected ArgumentsInvalidException
    }

    @Test
    public void testExecuteWithMoveExplicit() throws ArgumentsInvalidException,
            PlayerAlreadyExistsException,
            InvalidPlayerNameException,
            QuitGameException,
            CommandNotFoundException,
            PlayerNotFoundException,
            InvalidDiceNumberOrFormat {
        //Given
        String playerName = "Pippo";
        Integer dice1 = 4;
        Integer dice2 = 2;
        String input = "move " + playerName + " " + dice1 + "," + dice2;
        doNothing().when(gameContext).movePlayer(playerName, Arrays.asList(dice1, dice2));

        //When
        commandExecutor.execute(input);

        //Then
        verify(gameContext).movePlayer(playerName, Arrays.asList(dice1, dice2));

    }

    @Test
    public void testExecuteWithMoveImplicit() throws ArgumentsInvalidException,
            PlayerAlreadyExistsException,
            InvalidPlayerNameException,
            QuitGameException,
            CommandNotFoundException,
            PlayerNotFoundException,
            InvalidDiceNumberOrFormat {
        //Given
        String playerName = "Pippo";
        String input = "move " + playerName;
        doNothing().when(gameContext).movePlayer(playerName, null);

        //When
        commandExecutor.execute(input);

        //Then
        verify(gameContext).movePlayer(playerName, null);

    }

}