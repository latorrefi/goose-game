package com.latorrefi.goosegame.engine;

import com.latorrefi.goosegame.ouput.PrinterGameStatusListener;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
public class GameContextTest {

    @Mock
    Dice diceGenerator;

    @Mock
    PrinterGameStatusListener printerGameStatusListener;

    @Test
    public void testAddPlayer() throws Exception {
        //Given
        GameContext gameContext = new GameContext(printerGameStatusListener);
        String playerName = "Pippo";

        //When
        gameContext.addPlayer(playerName);

        //Then
        assertThat(gameContext.getPlayersName(), hasItem(playerName));
        verify(printerGameStatusListener).onPlayerAdd(gameContext.getPlayersName());
    }

    @Test(expected = PlayerAlreadyExistsException.class)
    public void testAddExistingPlayerThrowsException() throws Exception {
        //Given
        GameContext gameContext = new GameContext(printerGameStatusListener);
        String playerName = "Pippo";
        gameContext.addPlayer(playerName);

        //When
        gameContext.addPlayer(playerName);

        //Then
        //expected PlayerAlreadyExistsException
    }

    @Test(expected = InvalidPlayerNameException.class)
    public void testAddPlayerNull() throws Exception {
        //Given
        GameContext gameContext = new GameContext(printerGameStatusListener);
        //When
        gameContext.addPlayer(null);

        //Then
        //expected InvalidPlayerNameException
    }

    @Test(expected = PlayerNotFoundException.class)
    public void testMovePlayerKoPlayerNotFoundException() throws PlayerAlreadyExistsException, InvalidPlayerNameException, InvalidDiceNumberOrFormat, PlayerNotFoundException {
        //Given
        GameContext gameContext = new GameContext(printerGameStatusListener);
        String playerName = "Pippo";
        //When
        gameContext.movePlayer(playerName, Arrays.asList(1, 2));
        //Then
        //expected = PlayerNotFoundException
    }

    @Test(expected = InvalidDiceNumberOrFormat.class)
    public void testMovePlayerKoInvalidDiceNumberOrFormat() throws PlayerAlreadyExistsException, InvalidPlayerNameException, InvalidDiceNumberOrFormat, PlayerNotFoundException {
        //Given
        GameContext gameContext = new GameContext(printerGameStatusListener);
        String playerName = "Pippo";
        gameContext.addPlayer(playerName);
        //When
        gameContext.movePlayer(playerName, Arrays.asList(1, 9));
        //Then
        //expected = InvalidDiceNumberOrFormat
    }

    @Test
    public void testMovePlayerOk() throws PlayerAlreadyExistsException, InvalidPlayerNameException, InvalidDiceNumberOrFormat, PlayerNotFoundException {
        //Given
        GameContext gameContext = new GameContext(printerGameStatusListener);
        String playerName = "Pippo";
        gameContext.addPlayer(playerName);
        //When
        gameContext.movePlayer(playerName, Arrays.asList(1, 2));
        //Then
        //OK
        assertThat(gameContext.getPlayerSpaceIndex(playerName), equalTo(3));
        verify(printerGameStatusListener).onPlayerRolls(eq(playerName),
                anyList());
        verify(printerGameStatusListener).onPlayerMoved(eq(playerName),
                any(),
                any());
    }

    @Test
    public void testMovePlayerBounces() throws PlayerAlreadyExistsException, InvalidPlayerNameException, InvalidDiceNumberOrFormat, PlayerNotFoundException {
        //Given
        GameContext gameContext = new GameContext(printerGameStatusListener);
        String playerName = "Pippo";
        gameContext.addPlayer(playerName);
        //When
        gameContext.movePlayer(playerName, Arrays.asList(6, 6));
        gameContext.movePlayer(playerName, Arrays.asList(6, 6));
        gameContext.movePlayer(playerName, Arrays.asList(6, 6));
        gameContext.movePlayer(playerName, Arrays.asList(6, 6));
        gameContext.movePlayer(playerName, Arrays.asList(6, 6));
        gameContext.movePlayer(playerName, Arrays.asList(1, 3));
        //Then
        //OK
        assertThat(gameContext.getPlayerSpaceIndex(playerName), equalTo(62));
        verify(printerGameStatusListener, times(6)).onPlayerRolls(eq(playerName),
                anyList());
        verify(printerGameStatusListener, times(6)).onPlayerMoved(eq(playerName),
                any(),
                any());
        verify(printerGameStatusListener).onPlayerBounced(eq(playerName),
                any());
    }

    @Test
    public void testMovePlayerWin() throws PlayerAlreadyExistsException, InvalidPlayerNameException, InvalidDiceNumberOrFormat, PlayerNotFoundException {
        //Given
        GameContext gameContext = new GameContext(printerGameStatusListener);
        String playerName = "Pippo";
        gameContext.addPlayer(playerName);
        //When
        gameContext.movePlayer(playerName, Arrays.asList(6, 6));
        gameContext.movePlayer(playerName, Arrays.asList(6, 6));
        gameContext.movePlayer(playerName, Arrays.asList(6, 6));
        gameContext.movePlayer(playerName, Arrays.asList(6, 6));
        gameContext.movePlayer(playerName, Arrays.asList(6, 6));
        gameContext.movePlayer(playerName, Arrays.asList(1, 2));
        //Then
        //OK
        assertThat(gameContext.getPlayerSpaceIndex(playerName), equalTo(63));
        verify(printerGameStatusListener, times(6)).onPlayerRolls(eq(playerName),
                anyList());
        verify(printerGameStatusListener, times(6)).onPlayerMoved(eq(playerName),
                any(),
                any());
        verify(printerGameStatusListener).onPlayerWin(eq(playerName));
    }


    @Test
    public void testMovePlayerRandomly() throws PlayerAlreadyExistsException, InvalidPlayerNameException, InvalidDiceNumberOrFormat, PlayerNotFoundException {
        //Given
        GameContext gameContext = new GameContext(printerGameStatusListener, diceGenerator);
        String playerName = "Pippo";
        gameContext.addPlayer(playerName);
        given(diceGenerator.random()).willReturn(Arrays.asList(1, 2));

        //When
        gameContext.movePlayer(playerName, null);
        //Then
        //OK
        verify(diceGenerator).random();
        verify(printerGameStatusListener, times(1)).onPlayerRolls(eq(playerName),
                anyList());
        verify(printerGameStatusListener, times(1)).onPlayerMoved(eq(playerName),
                any(),
                any());
        assertThat(gameContext.getPlayerSpaceIndex(playerName), equalTo(3));
    }

    @Test
    public void testMovePlayerBridge() throws PlayerAlreadyExistsException, InvalidPlayerNameException, InvalidDiceNumberOrFormat, PlayerNotFoundException {
        //Given
        GameContext gameContext = new GameContext(printerGameStatusListener);
        String playerName = "Pippo";
        gameContext.addPlayer(playerName);
        //When
        gameContext.movePlayer(playerName, Arrays.asList(1, 3));
        gameContext.movePlayer(playerName, Arrays.asList(1, 1));
        //Then
        //OK
        assertThat(gameContext.getPlayerSpaceIndex(playerName), equalTo(12));
        verify(printerGameStatusListener, times(2)).onPlayerRolls(eq(playerName),
                anyList());
        verify(printerGameStatusListener, times(2)).onPlayerMoved(eq(playerName),
                any(),
                any());
        verify(printerGameStatusListener, times(1)).onPlayerJump(eq(playerName),
                any());
    }

    @Test
    public void testMovePlayerGooseSingleJump() throws PlayerAlreadyExistsException, InvalidPlayerNameException, InvalidDiceNumberOrFormat, PlayerNotFoundException {
        //Given
        GameContext gameContext = new GameContext(printerGameStatusListener);
        String playerName = "Pippo";
        gameContext.addPlayer(playerName);
        //When
        gameContext.movePlayer(playerName, Arrays.asList(1, 2));
        gameContext.movePlayer(playerName, Arrays.asList(1, 1));
        //Then
        //OK
        assertThat(gameContext.getPlayerSpaceIndex(playerName), equalTo(7));
        verify(printerGameStatusListener, times(2)).onPlayerRolls(eq(playerName),
                anyList());
        verify(printerGameStatusListener, times(2)).onPlayerMoved(eq(playerName),
                any(),
                any());
        verify(printerGameStatusListener, times(1)).onPlayerJump(eq(playerName),
                any());
    }

    @Test
    public void testMovePlayerGooseMultipleJump() throws PlayerAlreadyExistsException, InvalidPlayerNameException, InvalidDiceNumberOrFormat, PlayerNotFoundException {
        //Given
        GameContext gameContext = new GameContext(printerGameStatusListener);
        String playerName = "Pippo";
        gameContext.addPlayer(playerName);
        //When
        gameContext.movePlayer(playerName, Arrays.asList(1, 2));
        gameContext.movePlayer(playerName, Arrays.asList(1, 1));
        //Then
        //OK
        assertThat(gameContext.getPlayerSpaceIndex(playerName), equalTo(7));
        verify(printerGameStatusListener, times(2)).onPlayerRolls(eq(playerName),
                anyList());
        verify(printerGameStatusListener, times(2)).onPlayerMoved(eq(playerName),
                any(),
                any());
        verify(printerGameStatusListener, times(1)).onPlayerJump(eq(playerName),
                any());
    }
}