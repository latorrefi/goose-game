package com.latorrefi.goosegame.engine;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class DiceTest {


    @Test
    public void testRandom() {
        //Given
        Dice dice = new Dice();
        //When
        List<Integer> randomDice = dice.random();
        //Then
        assertThat(randomDice, hasSize(Dice.MAX_DICE_NUMBER));
        assertThat(randomDice.get(0), greaterThanOrEqualTo(1));
        assertThat(randomDice.get(0), lessThanOrEqualTo(Dice.MAX_DICE_VALUE));
        assertThat(randomDice.get(1), greaterThanOrEqualTo(1));
        assertThat(randomDice.get(1), lessThanOrEqualTo(Dice.MAX_DICE_VALUE));
    }

}